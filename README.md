# TM in science funding agencies

The project is analyzing the talent management strategy program at different science funding agencies. This repository contains the scripts to analyze the survey data and a description of the method to analyze the quantitative and qualitative data. 

For questions about the project please reach out to [Professor Alma McCarthy](mailto:almamccarthy@nuigalway.ie).
