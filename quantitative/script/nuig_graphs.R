# color schemes for NUIG graphs

stoplight2_na <- c('#91cf60', '#fc8d59', '#999999')
stoplight2_benchmark <- c('#91cf60', '#fc8d59', 'black')
stoplight3 <- c('#d73027', '#ffffbf', '#91cf60')
stoplight3_benchmark <- c('#91cf60', '#f6e85a',  '#fc8d59', 'black')
stoplight3_na <- c('#d73027', '#ffffbf', '#91cf60', '#999999')
mulberry <- rgb(104/255, 0, 92/255, maxColorValue=1)
mulberry75 <- rgb(104/255, 0/255, 92/255, alpha = 0.75, maxColorValue=1)
mulberry68 <- rgb(104/255, 0/255, 92/255, alpha = 0.68, maxColorValue=1)
midnightblue <- rgb(44, 60, 100, maxColorValue=255)
midnightblue75 <- rgb(44, 60, 100, alpha = 0.75*255, maxColorValue=255)
midnightblue50 <- rgb(44, 60, 100, alpha = 0.5*255, maxColorValue=255)
jade <- rgb(126, 169, 150, maxColorValue=255)
jade75 <- rgb(126, 169, 150, alpha = 0.75*255, maxColorValue=255)
duskblue <- rgb(127, 186, 201, maxColorValue=255)
duskblue75 <- rgb(127, 186, 201, alpha = 0.75*255, maxColorValue=255)

two_legends_color <- c(mulberry, midnightblue) # mulberry (yes), midnight purple (no)
two_legends_color_with_na <- c(mulberry, midnightblue, '#999999') # mulberry (yes), midnight purple (no)

three_legends_color <- c(mulberry, duskblue, midnightblue) #mulberry, pantone process 160-2, midnight purple
four_legends_color <- c(midnightblue, midnightblue75, mulberry75, mulberry)
four_legends_color_with_na <- c(duskblue, jade, midnightblue, mulberry, '#999999')
four_legends_color2 <- c(mulberry68, duskblue75, midnightblue75 , jade)

five_legends_color <- c(midnightblue, midnightblue75, duskblue, mulberry75, mulberry) # neg to pos anwers
five_legends_color_na <- c(duskblue, jade, midnightblue, mulberry68, mulberry, '#999999') # neg to pos anwers
six_legends_color <- c(midnightblue, midnightblue75, duskblue75, duskblue, mulberry68, mulberry) # neg to pos anwers
seven_legends_color <- c(midnightblue, midnightblue75, jade75, duskblue75, duskblue, mulberry75, mulberry) # neg to pos anwers

theme_nuig_bar_vertical <- function(size = 18, axistext = size - 4, legend.position='none', legendtitlesize = size-7, legendtextsize = size-8) {
  theme(panel.border = element_blank(),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        legend.position=legend.position, 
        legend.title=element_text(size=legendtitlesize), 
        legend.text=element_text(size=legendtextsize),
        
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        axis.ticks = element_blank(),
        axis.ticks.length = unit(-0.3, "cm"),
        axis.text.y = element_blank(),
        axis.text = element_text(size = axistext),
        
        text = element_text(size=18)
  )
}

theme_nuig_bar_horizontal <- function(size = 18, legend.position='none', legendtextsize = 10, legendtitlesize = 9) {
  theme(panel.border = element_blank(),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        legend.position=legend.position, 
        legend.title=element_text(size=legendtitlesize), 
        legend.text=element_text(size=legendtextsize),
        axis.title.y = element_blank(),
        axis.title.x = element_blank(),
        axis.ticks = element_blank(),
        axis.ticks.length = unit(-0.3, "cm"),
        axis.text.x = element_blank(),
        text = element_text(size=size)
  )
}